import { Component, OnInit } from '@angular/core';
import {CommentService} from '../core/services/comment.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent implements OnInit {

  constructor(private commentService: CommentService) { }

  ngOnInit() {
  }

  get allComments() {
    return this.commentService.getAllComments();
  }
}
