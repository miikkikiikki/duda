import {Component, Input, OnInit} from '@angular/core';
import {Comment} from '../core/models/comment.model';
import {CommentService} from '../core/services/comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() comment: Comment;

  constructor(private commentService: CommentService) { }

  ngOnInit() {
  }

  removeComment(id) {
    this.commentService.deleteCommentById(id);
  }

  editComment(comment) {
    this.commentService.setTransferData(comment);
  }

}
