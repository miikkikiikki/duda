import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentService} from '../core/services/comment.service';
import {Comment} from '../core/models/comment.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  isUpdate = false;
  updateId: number;
  commentForm: FormGroup;

  constructor(private fb: FormBuilder,
              private commentService: CommentService) {
    this.commentService.getTransferData.subscribe(comment => {
      if (Object.keys(comment).length > 0) {
        this.isUpdate = true;
        this.updateId = comment.id;
        this.commentForm.controls['name'].setValue(comment.name);
        this.commentForm.controls['text'].setValue(comment.comment);
      }
    });
  }

  ngOnInit() {
    this.commentForm = this.fb.group({
      name: ['', Validators.required],
      text: ['', Validators.required]
    });
  }

  onSubmit() {
    const comment: Comment = new Comment();
    comment.name = this.commentForm.controls['name'].value;
    comment.comment = this.commentForm.controls['text'].value;
    this.commentService.addComment(comment);
    this.commentForm.reset();
  }

  onSave() {
    const comment: Comment = new Comment();
    comment.name = this.commentForm.controls['name'].value;
    comment.comment = this.commentForm.controls['text'].value;
    this.commentService.updateCommentById(this.updateId, comment);
    this.commentForm.reset();
    this.isUpdate = false;
  }



}
