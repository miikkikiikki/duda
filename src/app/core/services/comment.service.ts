import { Injectable } from '@angular/core';
import {Comment} from '../models/comment.model';
import {BehaviorSubject} from 'rxjs';
import * as imgGen from '@dudadev/random-img';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  lastId = 0;
  comments: Comment[] = [];
  private transferData = new BehaviorSubject<any>([]);

  constructor() {}

  addComment(comment: Comment) {
    if (!comment.id) {
      comment.id = ++this.lastId;
    }
    imgGen().then(avatarURL => {
      comment.image = avatarURL;
      this.comments.push(comment);
      return this;
    });
  }

  deleteCommentById(id: number) {
    this.comments = this.comments
      .filter(comment => comment.id !== id);
    return this;
  }

  updateCommentById(id: number, values) {
    const todo = this.getCommentById(id);
    if (!todo) {
      return null;
    }
    Object.assign(todo, values);
    return todo;
  }

  getAllComments() {
    return this.comments;
  }

  getCommentById(id: number) {
    return this.comments
      .filter(todo => todo.id === id)
      .pop();
  }

  setTransferData(data) {
    this.transferData.next(data);
  }

  get getTransferData() {
    return this.transferData.asObservable();
  }

}
